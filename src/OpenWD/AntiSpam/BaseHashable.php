<?php

namespace OpenWD\AntiSpam;

trait BaseHashable
{
    private $target;
    private $title;
    private $type = '';
    private $rotateTitle = true;

    public function __construct($target = null, $title = null)
    {
        $this->set($target, $title);
        return $this;
    }

    public function set($target = null, $title = null)
    {
        $this->setTarget($target);
        if ($title == null) {
            $this->setTitle($target);
            return $this;
        }
        $this->setTitle($title);
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    public function html()
    {
        $id = 'rot_' . uniqid();
        $content = $this->rotateTitle ? '' : $this->title;
        $html = '<span id="' . $id . '" data-type="' . $this->type . '" data-target="' . \base64_encode($this->target) . '" data-title="' . \base64_encode($this->title) . '">' . $content . '</span>';

        $script = "<script>
        var el_{$id} = document.getElementById('{$id}');
        var title_{$id} = atob(el_{$id}.dataset.title);
        var target_{$id} = atob(el_{$id}.dataset.target);
        var type_{$id} = el_{$id}.dataset.type;
        type_{$id} = type_{$id}.length ? type_{$id}+':' : '';
        var link_{$id} = document.createElement('a');
        link_{$id}.innerHTML = title_{$id};
        link_{$id}.href = type_{$id} + target_{$id};
        el_{$id}.appendChild(link_{$id});
        </script>";

        return $html . $script;
    }

    public function show()
    {
        echo $this->html();
        return $this;
    }
}
