<?php

namespace OpenWD\AntiSpam;

class PhoneBaseHash
{
    use BaseHashable;
    
    public function __construct($target = null, $title = null)
    {
        $this->type = 'tel';
        $this->set($target, $title);
        return $this;
    }
}
