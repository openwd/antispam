<?php

namespace OpenWD\AntiSpam;

class Honeypot
{
    private $fields = [];
    private $prefix = '';
    private $suffix = '';

    public function __construct($fields=null, $prefix=null, $suffix=null)
    {
        $this->fields = is_array($fields) ? $fields : [
            'website',
            'email',
            'message',
        ];
        $this->suffix = is_string($suffix) ? $suffix : '_hp';
        $this->prefix = is_string($prefix) ? $prefix : '';
    }

    public function fields() {
        return array_map(function($field) {
            return $this->prefix . $field . $this->suffix;
        }, $this->fields);
    }

    public function html()
    {
        $html = '';
        $ids = [];
        foreach ($this->fields() as $field) {
            $id = $field.'_'.uniqid();
            $html .= '<input type="text" name="' . $field . '" id="' . $id . '" value="">';
            $ids[] = '#'.$id;
        }
        $style = '<style>'.implode(', ', $ids) . '{ display: none; }</style>';
        return $style.$html;
    }

    public function show()
    {
        echo $this->html();
    }

    public function validate() {
        foreach($this->fields() as $field) {
            if (!empty($_POST[$field])) {
                return false;
            }
        }
        return true;
    }
}
