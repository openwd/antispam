<?php

namespace OpenWD\AntiSpam;

class MailBaseHash
{
    use BaseHashable;

    public function __construct($target = null, $title = null)
    {
        $this->type = 'mailto';
        $this->set($target, $title);
        return $this;
    }
}
