<?php

use OpenWD\AntiSpam\Honeypot;

include('../vendor/autoload.php');

$honeypot = new Honeypot(['test1', 'test2', 'test3'], 'prefix_', '-suffix');

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Honeypot test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <?php
    if (!empty($_POST)) {
        echo '<pre>';
        print_r($_POST);
        echo '</pre>';
        $valid = $honeypot->validate();
        var_dump($valid);
    }

    ?>
    <form action="" method="post" class="container my-5">
        <?php
            $honeypot->show();
        ?>
        <input type="text" name="firstname" id="firstname" class="form-control">
        <input type="text" name="lastname" id="lastname" class="form-control">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</body>

</html>