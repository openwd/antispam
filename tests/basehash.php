<?php

use OpenWD\AntiSpam\MailBaseHash;
use OpenWD\AntiSpam\PhoneBaseHash;

include('../vendor/autoload.php');

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BaseHash test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
<?php
$mailBaseHash = new MailBaseHash('test@example.com');
$mailBaseHash->show();
echo "<br>\n";

$mailBaseHash = new MailBaseHash('test@example.com', 'Mail link title');
$mailBaseHash->show();
echo "<br>\n";

$mailBaseHash = new MailBaseHash();
$mailBaseHash->setTarget('test@example.com');
$mailBaseHash->setTitle('Mail link title from setTitle()');
$mailBaseHash->show();
echo "<br>\n";

$phoneBaseHash = new PhoneBaseHash('+31600000000');
$phoneBaseHash->show();
echo "<br>\n";

$phoneBaseHash = new PhoneBaseHash('+31600000000', 'Phone link title');
$phoneBaseHash->show();
echo "<br>\n";
?>
</body>

</html>