# OpenWD AntiSpam

[![Latest Version on Packagist](https://img.shields.io/packagist/v/openwd/antispam.svg?style=flat-square)](https://packagist.org/packages/openwd/antispam)
[![MIT Licensed](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Total Downloads](https://img.shields.io/packagist/dt/openwd/antispam.svg?style=flat-square)](https://packagist.org/packages/openwd/antispam)

## Installation

You can install the package via composer:

``` bash
composer require openwd/antispam
```

## Honeypot

A simple honeypot to validate forms against bots

```php
use OpenWD\AntiSpam\Honeypot;

$honeypot = new Honeypot();
$honeypot->show();
$valid = $honeypot->validate();
var_dump($valid);
```

## BaseHash

Base64 encode links and decode them with javascript to prevent simple crawlers from fetching links like phone numbers or email addresses.

```php
use OpenWD\AntiSpam\MailBaseHash;
use OpenWD\AntiSpam\PhoneBaseHash;

//A mail address
$mailBaseHash = new MailBaseHash('test@example.com');
$html = $mailBaseHash->html(); //Get the BaseHash html
$mailBaseHash->show(); //Echo the BaseHash html

//A mail address with a title
$mailBaseHash = new MailBaseHash('test@example.com', 'Mail link title');

//setTarget() and setTitle()
$mailBaseHash = new MailBaseHash();
$mailBaseHash->setTarget('test@example.com');
$mailBaseHash->setTitle('Mail link title from setTitle()');

//A phone number
$phoneBaseHash = new PhoneBaseHash('+31600000000');

//A phone number with a title
$phoneBaseHash = new PhoneBaseHash('+31600000000', 'Phone link title');
```